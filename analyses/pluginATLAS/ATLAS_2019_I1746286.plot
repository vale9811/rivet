# BEGIN PLOT /ATLAS_2019_I1746286/.*
LegendAlign=r
# END PLOT

# BEGIN PLOT /ATLAS_2019_I1746286/d01-x01-y01
Title= $p_\text{T}$ for $K^{0}_{S}$ inside $b$-jets
XLabel=$p_\text{T}$ [GeV]
YLabel=$(1/N_\text{evt}) \text{d}N_{K}/\text{d}p_\text{T}$ [GeV$^{-1}$]
# END PLOT

# BEGIN PLOT /ATLAS_2019_I1746286/d04-x01-y01
Title= $|\eta|$ for $K^{0}_{S}$ inside $b$-jets
XLabel=$|\eta|$
YLabel=$(1/N_\text{evt}) \text{d}N_{K}/\text{d}|\eta|$
# END PLOT

# BEGIN PLOT /ATLAS_2019_I1746286/d03-x01-y01
Title= Energy for $K^{0}_{S}$ inside $b$-jets
XLabel=$E$ [GeV]
YLabel=$(1/N_\text{evt}) \text{d}N_{K}/\text{d}E$ [GeV$^{-1}$]
# END PLOT

# BEGIN PLOT /ATLAS_2019_I1746286/d05-x01-y01
Title= Multiplicity for $K^{0}_{S}$ inside $b$-jets
XLabel=$N_{K}$
YLabel=$(1/N_\text{evt}) \text{d}N_\text{evt}/\text{d}N_{K}$
# END PLOT

# BEGIN PLOT /ATLAS_2019_I1746286/d02-x01-y01
Title= $x_{K}$ for $K^{0}_{S}$ inside $b$-jets
XLabel=$x_{K}$
YLabel=$(1/N_\text{evt}) \text{d}N_{K}/dx_{K}$
# END PLOT

# BEGIN PLOT /ATLAS_2019_I1746286/d01-x02-y01
Title= $p_\text{T}$ for $K^{0}_{S}$ inside non-$b$-jets
XLabel=$p_\text{T}$ [GeV]
YLabel=$(1/N_\text{evt}) \text{d}N_{K}/\text{d}p_\text{T}$ [GeV$^{-1}$]
# END PLOT

# BEGIN PLOT /ATLAS_2019_I1746286/d01-x02-y02
Title= $|\eta|$ for $K^{0}_{S}$ inside non-$b$-jets
XLabel=$|\eta|$
YLabel=$(1/N_\text{evt}) \text{d}N_{K}/\text{d}|\eta|$
# END PLOT

# BEGIN PLOT /ATLAS_2019_I1746286/d01-x02-y03
Title= Energy for $K^{0}_{S}$ inside non-$b$-jets
XLabel=$E$ [GeV]
YLabel=$(1/N_\text{evt}) \text{d}N_{K}/\text{d}E$ [GeV$^{-1}$]
# END PLOT

# BEGIN PLOT /ATLAS_2019_I1746286/d01-x02-y04
Title= Multiplicity for $K^{0}_{S}$ inside non-$b$-jets
XLabel=$N_{K}$
YLabel=$(1/N_\text{evt}) \text{d}N_\text{evt}/\text{d}N_{K}$
# END PLOT

# BEGIN PLOT /ATLAS_2019_I1746286/d01-x02-y05
Title= $x_{K}$ for $K^{0}_{S}$ inside non-$b$-jets
XLabel=$x_{K}$
YLabel=$(1/N_\text{evt}) \text{d}N_{K}/dx_{K}$
# END PLOT

# BEGIN PLOT /ATLAS_2019_I1746286/d01-x03-y01
Title= $p_\text{T}$ for $K^{0}_{S}$ outside jets
XLabel=$p_\text{T}$ [GeV]
YLabel=$(1/N_\text{evt}) \text{d}N_{K}/\text{d}p_\text{T}$ [GeV$^{-1}$]
# END PLOT

# BEGIN PLOT /ATLAS_2019_I1746286/d01-x03-y02
Title= $|\eta|$ for $K^{0}_{S}$ outside jets
XLabel=$|\eta|$
YLabel=$(1/N_\text{evt}) \text{d}N_{K}/\text{d}|\eta|$
# END PLOT

# BEGIN PLOT /ATLAS_2019_I1746286/d01-x03-y03
Title= Energy for $K^{0}_{S}$ outside jets
XLabel=$E$ [GeV]
YLabel=$(1/N_\text{evt}) \text{d}N_{K}/\text{d}E$ [GeV$^{-1}$]
# END PLOT

# BEGIN PLOT /ATLAS_2019_I1746286/d01-x03-y04
Title= Multiplicity for $K^{0}_{S}$ outside jets
XLabel=$N_{K}$
YLabel=$(1/N_\text{evt}) \text{d}N_\text{evt}/\text{d}N_{K}$
# END PLOT


# BEGIN PLOT /ATLAS_2019_I1746286/d01-x04-y01
Title= $p_\text{T}$ for $K^{0}_{S}$ total sample
XLabel=$p_\text{T}$ [GeV]
YLabel=$(1/N_\text{evt}) \text{d}N_{K}/\text{d}p_\text{T}$ [GeV$^{-1}$]
# END PLOT

# BEGIN PLOT /ATLAS_2019_I1746286/d01-x04-y02
Title= $|\eta|$ for $K^{0}_{S}$ total sample
XLabel=$|\eta|$
YLabel=$(1/N_\text{evt}) \text{d}N_{K}/\text{d}|\eta|$
# END PLOT

# BEGIN PLOT /ATLAS_2019_I1746286/d01-x04-y03
Title= Energy for $K^{0}_{S}$ total sample
XLabel=$E$ [GeV]
YLabel=$(1/N_\text{evt}) \text{d}N_{K}/\text{d}E$ [GeV$^{-1}$]
# END PLOT

# BEGIN PLOT /ATLAS_2019_I1746286/d01-x04-y04
Title= Multiplicity for $K^{0}_{S}$ total sample
XLabel=$N_{K}$
YLabel=$(1/N_\text{evt}) \text{d}N_\text{evt}/\text{d}N_{K}$
# END PLOT

# BEGIN PLOT /ATLAS_2019_I1746286/d02-x04-y01
Title= $p_\text{T}$ for $\Lambda$ total sample
XLabel=$p_\text{T}$ [GeV]
YLabel=$(1/N_\text{evt}) \text{d}N_{\Lambda}/\text{d}p_\text{T}$ [GeV$^{-1}$]
# END PLOT

# BEGIN PLOT /ATLAS_2019_I1746286/d02-x04-y02
Title= $|\eta|$ for $\Lambda$ total sample
XLabel=$|\eta|$
YLabel=$(1/N_\text{evt}) \text{d}N_{\Lambda}/\text{d}|\eta|$
# END PLOT

# BEGIN PLOT /ATLAS_2019_I1746286/d02-x04-y03
Title= Energy for $\Lambda$ total sample
XLabel=$E$ [GeV]
YLabel=$(1/N_\text{evt}) \text{d}N_{\Lambda}/\text{d}E$ [GeV$^{-1}$]
# END PLOT


