BEGIN PLOT /BABAR_2007_I746745/d01-x01-y01
Title=Spectrum for $\Omega_c^{0}$ production
XLabel=$p$ [GeV]
YLabel=$1/N\text{d}N/\text{d}p$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
