BEGIN PLOT /BELLE_2008_I762013/d01-x01-y01
Title=Decay angle for $D_{s1}(2536)^+\to D^{*+} K^0$
XLabel=$\cos\alpha$
YLabel=$1/N\text{d}N/\text{d}\cos\alpha$
LogY=0
END PLOT
BEGIN PLOT /BELLE_2008_I762013/d02-x01-y01
Title=$\beta$ angle for $D_{s1}(2536)^+\to D^{*+} K^0$
XLabel=$\beta$
YLabel=$1/N\text{d}N/\text{d}\beta$
LogY=0
END PLOT
BEGIN PLOT /BELLE_2008_I762013/d03-x01-y01
Title=Helicity angle for $D_{s1}(2536)^+\to D^{*+}(\to D^0\pi^+) K^0$
XLabel=$\cos\gamma$
YLabel=$1/N\text{d}N/\text{d}\cos\gamma$
LogY=0
END PLOT
