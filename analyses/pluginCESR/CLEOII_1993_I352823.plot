BEGIN PLOT /CLEOII_1993_I352823/d03-x01-y01
Title=Spectrum for $D_{s1}(2536)^+$ production
XLabel=$x_p$
YLabel=$1/N\text{d}N/\text{d}x_p$
LogY=0
END PLOT
BEGIN PLOT /CLEOII_1993_I352823/d04-x01-y01
Title=Helicity angle for $D_{s1}(2536)^+\to D^{*0}(\to D^0\pi^0)K^+$
XLabel=$\cos\theta$
YLabel=$1/N\text{d}N/\text{d}\cos\theta$
LogY=0
END PLOT
