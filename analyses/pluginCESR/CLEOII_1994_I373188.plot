BEGIN PLOT /CLEOII_1994_I373188/d01-x01-y01
Title=$ K^-\eta$ mass in $\tau^-\to 3\pi^-2\pi^+\pi^0\nu_\tau$ decays
XLabel=$m_{ K^-\eta}$ [$\text{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{3\pi^-2\pi^+\pi^0}$ [ $\text{GeV}^{-1}$]
LogY=0
END PLOT
