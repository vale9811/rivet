BEGIN PLOT /CLEO_1992_I315181/d01-x01-y01
Title=Proton momentum spectrum
XLabel=$p$ [GeV]
YLabel=$1/2/N_{B\bar{B}}\text{d}N/\text{d}p$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /CLEO_1992_I315181/d02-x01-y01
Title=$\Lambda_c^+$ momentum spectrum
XLabel=$p$ [GeV]
YLabel=$1/2/N_{B\bar{B}}\text{d}N/\text{d}p\times B(\Lambda_c^+\top K^-\pi^+)$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /CLEO_1992_I315181/d03-x01-y01
Title=$\Lambda^0$ momentum spectrum
XLabel=$p$ [GeV]
YLabel=$1/2/N_{B\bar{B}}\text{d}N/\text{d}p$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /CLEO_1992_I315181/d04-x01-y01
Title=$\Xi^-$ momentum spectrum
XLabel=$p$ [GeV]
YLabel=$1/2/N_{B\bar{B}}\text{d}N/\text{d}p$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
