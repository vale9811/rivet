BEGIN PLOT /ARGUS_1991_I315059/d01-x01-y01
Title=Cross Section for $D^0$, $\bar{D}^0$ production (continuum)
YLabel=$\sigma(D^0, \bar{D}^0)$ [nb]
LogY=0
END PLOT
BEGIN PLOT /ARGUS_1991_I315059/d01-x01-y02
Title=Cross Section for $D^{\pm}$ production (continuum)
YLabel=$\sigma(D^\pm)$ [nb]
LogY=0
END PLOT
BEGIN PLOT /ARGUS_1991_I315059/d01-x01-y03
Title=Cross Section for $D^{*\pm}$ production (continuum)
YLabel=$\sigma(D^{*\pm})$ [nb]
LogY=0
END PLOT
BEGIN PLOT /ARGUS_1991_I315059/d02-x01-y01
Title=Scaled momentum spectrum for $D^0$, $\bar{D}^0$ production (continuum)
YLabel=$\text{Br}(D^0\toK^-\pi^+) s \text{d}\sigma/\text{d}x_p$ [$\text{nb}\times\text{GeV}^2$]
XLabel=$x_p$
LogY=0
END PLOT
BEGIN PLOT /ARGUS_1991_I315059/d03-x01-y01
Title=Scaled momentum spectrum for $D^\pm$ production (continuum)
YLabel=$\text{Br}(D^+\toK^-\pi^+\pi^+) s \text{d}\sigma/\text{d}x_p$ [$\text{nb}\times\text{GeV}^2$]
XLabel=$x_p$
LogY=0
END PLOT
BEGIN PLOT /ARGUS_1991_I315059/d04-x01-y01
Title=Scaled momentum spectrum for $D^{*\pm}$ production (continuum)
YLabel=$\text{Br}(D^{*+}\toD^0\pi^+)\text{Br}(D^0\toK^-\pi^+) s \text{d}\sigma/\text{d}x_p$ [$\text{nb}\times\text{GeV}^2$]
XLabel=$x_p$
LogY=0
END PLOT
BEGIN PLOT /ARGUS_1991_I315059/d05-x01-y01
Title=Momentum spectrum for $D^0$, $\bar{D}^0$ production (B decay)
XLabel=$p$ [GeV]
YLabel=$\text{Br}(D^0\toK^-\pi^+)/N_B\text{d}N_{D^0}/\text{d}p$ [$10^{-3}/\text{GeV}$]
LogY=0
END PLOT
BEGIN PLOT /ARGUS_1991_I315059/d06-x01-y01
Title=Momentum spectrum for $D^{\pm}$ production (B decay)
XLabel=$p$ [GeV]
YLabel=$\text{Br}(D^+\toK^-\pi^+\pi^+)/N_B\text{d}N_{D^\pm}/\text{d}p$ [$10^{-3}/\text{GeV}$]
LogY=0
END PLOT
BEGIN PLOT /ARGUS_1991_I315059/d07-x01-y01
Title=Momentum spectrum for $D^{*\pm}$ production (B decay)
XLabel=$p$ [GeV]
YLabel=$\text{Br}(D^{*+}\toD^0\pi^+)\text{Br}(D^0\toK^-\pi^+)/N_B\text{d}N_{D^{*\pm}}/\text{d}p$ [$10^{-3}/\text{GeV}$]
LogY=0
END PLOT