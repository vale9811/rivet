BEGIN PLOT /OPAL_2001_I536266/d01-x01-y01
Title=Charged particle multiplicity ($u\bar u$) 
YLabel=$\langle N_{\text{charged}}\rangle$
LogY=0
END PLOT
BEGIN PLOT /OPAL_2001_I536266/d01-x01-y02
Title=Charged particle multiplicity ($d\bar d$) 
YLabel=$\langle N_{\text{charged}}\rangle$
LogY=0
END PLOT
BEGIN PLOT /OPAL_2001_I536266/d01-x01-y03
Title=Charged particle multiplicity ($s\bar s$) 
YLabel=$\langle N_{\text{charged}}\rangle$
LogY=0
END PLOT
BEGIN PLOT /OPAL_2001_I536266/d02-x01-y01
Title=Charged particle multiplicity ratio ($u\bar u/d\bar d$) 
YLabel=$\langle N^{\text{up}}_{\text{charged}}\rangle/\langle N^{\text{down}}_{\text{charged}}\rangle$
LogY=0
END PLOT
BEGIN PLOT /OPAL_2001_I536266/d02-x01-y02
Title=Charged particle multiplicity ratio ($s\bar s/d\bar d$) 
YLabel=$\langle N^{\text{strange}}_{\text{charged}}\rangle/\langle N^{\text{down}}_{\text{charged}}\rangle$
LogY=0
END PLOT
BEGIN PLOT /OPAL_2001_I536266/d02-x01-y03
Title=Charged particle multiplicity ratio ($s\bar s/u\bar u$) 
YLabel=$\langle N^{\text{strange}}_{\text{charged}}\rangle/\langle N^{\text{up}}_{\text{charged}}\rangle$
LogY=0
END PLOT
